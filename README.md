# Flask Hello World App (DevSecOps Lab)

This is an application used as part of the DevSecOps lab. 

The lab exercises can be found [here](https://gitlab.com/gtpe-dso-lab/dso-exercises).

## Materials

### Flask Hello World Application

The exemplar project is a very basic Python [Flask](https://flask.palletsprojects.com/en/1.1.x/) application that exposes a few endpoints.

### Pipeline Template

The [pipeline templates](https://gitlab.com/gtpe-dso-lab/dso-deployment) are a centralized set of [Gitlab-CI](https://docs.gitlab.com/ee/ci/) stages to be employed by an application to allow for continuous deployment based on branch-based success.

### Deployment Configuration

The deployment configuration is a set of Kubernetes configs, modified by [Kustomize](https://kustomize.io/) settings, specific to each environment - staging and production.  The configs are used by [ArgoCD](https://argoproj.github.io/argo-cd/) which is running in the target cluster to deploy the application appropriately when it changes.

## Exercises

See [lab_exercises](https://gitlab.com/gtpe-dso-lab/dso-exercises).

## Running locally with Docker Desktop

If you want to run the app locally, you can do the following.

```
docker build -t flask_helloworld .
docker run -it -p 5000:5000 flask_helloworld:latest
```

Now you can go to http://0.0.0.0:5000/ on your host machine and you should see the app. 

<change>
